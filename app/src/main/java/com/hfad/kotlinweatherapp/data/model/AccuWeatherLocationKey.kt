package com.hfad.kotlinweatherapp.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AccuWeatherLocationKey {

    @SerializedName("Version")
    @Expose
    var Version:Int? = null

    @SerializedName("Key")
    @Expose
    var Key:String? = null

    @SerializedName("Type")
    @Expose
    var Type:String? = null

    @SerializedName("Rank")
    @Expose
    var Rank:Int? = null

    @SerializedName("LocalizedName")
    @Expose
    var LocalizedName:String? = null

    @SerializedName("EnglishName")
    @Expose
    var EnglishName:String? = null

    @SerializedName("PrimaryPostalCode")
    @Expose
    var PrimaryPostalCode:String? = null

    @SerializedName("Region")
    @Expose
    var region: Region? = null

    @SerializedName("Country")
    @Expose
    var country: Country? = null

    @SerializedName("AdministrativeArea")
    @Expose
    var administrativeArea: AdministrativeArea? = null

    @SerializedName("TimeZone")
    @Expose
    var timeZone: TimeZone? = null

    @SerializedName("GeoPosition")
    @Expose
    var geoPosition: GeoPosition? = null

    @SerializedName("IsAlias")
    @Expose
    var IsAlias:Boolean? = null

    @SerializedName("SupplementalAdminAreas")
    @Expose
    private var supplementalAdminAreas: List<Object>? = null

    @SerializedName("DataSets")
    @Expose
    var dataSets: List<Object>? = null


}