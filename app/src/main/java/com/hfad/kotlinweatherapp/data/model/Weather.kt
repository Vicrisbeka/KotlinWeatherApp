package com.hfad.kotlinweatherapp.data.model

import com.hfad.kotlinweatherapp.utils.common.Common

class Weather {
    var cityName:String?=null
    var iconDay:String?=null
    var iconNight:String?=null
    var iconPhraseDay:String?=null
    var iconPhraseNight:String?=null
    var temperatureMax:String?=null
    var temperatureMin:String?=null
    var dateTime:String?=null

    constructor(
        cityName: String?,
        iconDay: String?,
        iconNight: String?,
        iconPhraseDay: String?,
        iconPhraseNight: String?,
        temperatureMax: String?,
        temperatureMin: String?,
        dateTime: String?
    ) {
        this.cityName = cityName
        this.iconDay = makeUrlForIcon(iconDay!!)
        this.iconNight = makeUrlForIcon(iconNight!!)
        this.iconPhraseDay = iconPhraseDay
        this.iconPhraseNight = iconPhraseNight
        this.temperatureMax = convertFahrenheitToCelcium(temperatureMax!!)
        this.temperatureMin = convertFahrenheitToCelcium(temperatureMin!!)
        this.dateTime = convertDateFormat(dateTime!!)
    }

    fun makeUrlForIcon(icon:String):String{
        var iconUrl:String
        if (icon.toInt()<10) {
            iconUrl = Common.iconUrl + "0" + icon + "-s.png"
        } else {
            iconUrl = Common.iconUrl + icon + "-s.png"
        }
        return iconUrl
    }

    fun convertFahrenheitToCelcium(temperature:String):String{
        var temperatureCelcium:String

        temperatureCelcium = ((Math.round((temperature.toDouble()-32)*5/9)*100.0)/100.0).toString()+" °C"
        return temperatureCelcium

    }

    fun convertDateFormat(dateTime:String):String{
//        var inputFormat = SimpleDateFormat("EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.ENGLISH)
//        inputFormat.timeZone = TimeZone.getTimeZone("Etc/UTC")
//        var outputFormat = SimpleDateFormat("MMM dd, yyyy h:mm a")
//
//        var date = inputFormat.parse(dateTime)
//        var outputText = outputFormat.format(date)

        return dateTime
    }


}