package com.hfad.kotlinweatherapp.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AdministrativeArea {

    @SerializedName("ID")
    @Expose
    var ID:String? = null

    @SerializedName("LocalizedName")
    @Expose
    var LocalizedName:String? = null

    @SerializedName("EnglishName")
    @Expose
    var EnglishName:String? = null

    @SerializedName("Level")
    @Expose
    var Level:Int? = null

    @SerializedName("LocalizedType")
    @Expose
    var LocalizedType:String? = null

    @SerializedName("EnglishType")
    @Expose
    var EnglishType:String? = null

    @SerializedName("CountryID")
    @Expose
    var CountryID:String? = null
}

class Country {

    @SerializedName("ID")
    @Expose
    var ID:String? = null

    @SerializedName("LocalizedName")
    @Expose
    var LocalizedName:String? = null

    @SerializedName("EnglishName")
    @Expose
    var EnglishName:String? = null
}

class GeoPosition {

    @SerializedName("Latitude")
    @Expose
    var Latitude:Double? = null

    @SerializedName("Longitude")
    @Expose
    var Longitude:Double? = null

    @SerializedName("Elevation")
    @Expose
    var elevation: Elevation? = null
}

class Elevation {

    @SerializedName("Metric")
    @Expose
    var metric: Metric? = null

    @SerializedName("Imperial")
    @Expose
    var imperial: Imperial? = null
}

class Metric {

    @SerializedName("Value")
    @Expose
    var Value:Float? = null

    @SerializedName("Unit")
    @Expose
    var Unit:String? = null

    @SerializedName("UnitType")
    @Expose
    var UnitType:Int? = null
}

class Imperial {

    @SerializedName("Value")
    @Expose
    var Value:Float? = null

    @SerializedName("Unit")
    @Expose
    var Unit:String? = null

    @SerializedName("UnitType")
    @Expose
    var UnitType:Int? = null
}

class Region{

    @SerializedName("ID")
    @Expose
    var ID:String? = null

    @SerializedName("LocalizedName")
    @Expose
    var LocalizedName:String? = null

    @SerializedName("EnglishName")
    @Expose
    var EnglishName:String? = null

}

class TimeZone {

    @SerializedName("Code")
    @Expose
    var Code:String? = null

    @SerializedName("Name")
    @Expose
    var Name:String? = null

    @SerializedName("GmtOffset")
    @Expose
    var GmtOffset:Float? = null

    @SerializedName("IsDaylightSaving")
    @Expose
    var IsDaylightSaving:Boolean? = null

    @SerializedName("NextOffsetChange")
    @Expose
    var NextOffsetChange:String? = null
}