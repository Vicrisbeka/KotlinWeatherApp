package com.hfad.kotlinweatherapp.data.network

import com.hfad.kotlinweatherapp.data.model.AccuWeatherLocationKey
import com.hfad.kotlinweatherapp.data.model.AccuWeatherLocationWeather
import com.hfad.kotlinweatherapp.utils.common.Common
import retrofit2.Retrofit
import retrofit2.Callback
import retrofit2.converter.gson.GsonConverterFactory


class NetworkManager {

    companion object {
        private var instance: NetworkManager? = null
        fun getInstance(): NetworkManager? {
            if (instance == null) {
                instance =
                    NetworkManager()
            }
            return instance
        }
    }

    private fun api(): Api = Retrofit.Builder().baseUrl(Common.url)
        .addConverterFactory(GsonConverterFactory.create())
        .build().create(Api::class.java)

    fun getKeyMethod(location:String, cb: Callback<AccuWeatherLocationKey>) {
        api().getKey(Common.api, location = location).enqueue(cb)
    }

    fun getLocationWeather(locationKey: String, cb: Callback<AccuWeatherLocationWeather>) {
        api().getLocationWeather(locationKey, Common.api,"en-EN").enqueue(cb)
    }

    fun getKeyMethodByCityName(cityName: String, cb: Callback<ArrayList<AccuWeatherLocationKey>>) {
        api().getKeyByCityName(Common.api, cityName).enqueue(cb)
    }
}