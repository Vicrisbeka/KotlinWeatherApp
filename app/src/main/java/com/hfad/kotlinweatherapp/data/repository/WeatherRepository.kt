package com.hfad.kotlinweatherapp.data.repository

import android.location.Location
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.hfad.kotlinweatherapp.data.model.AccuWeatherLocationKey
import com.hfad.kotlinweatherapp.data.model.AccuWeatherLocationWeather
import com.hfad.kotlinweatherapp.data.model.DailyForecast
import com.hfad.kotlinweatherapp.data.model.Weather
import com.hfad.kotlinweatherapp.data.network.NetworkManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherRepository {
    private var weatherRepository: WeatherRepository? = null

    fun getInstance(): WeatherRepository? {
        if (weatherRepository == null) {
            weatherRepository = WeatherRepository()
        }
        return weatherRepository
    }

    fun getWeather(location: Location?, mutableLiveData: MutableLiveData<ArrayList<Weather>>?) {
        if (location == null) {
            return getKeyMethod("55.751244, 37.618423", mutableLiveData)
        } else {
            val lon = location.longitude.toString()
            val lat = location.latitude.toString()
            return getKeyMethod("" + lat + ", " + lon, mutableLiveData)
        }
    }

    fun getWeatherByCity(cityName: String, mutableLiveData: MutableLiveData<ArrayList<Weather>>?) {
        if (cityName.isEmpty() || cityName.isBlank()) {
            getKeyMethodByCityName("Bishkek", mutableLiveData)
        } else {
            getKeyMethodByCityName(cityName, mutableLiveData)
        }
    }

    fun getKeyMethod(location: String, mutableLiveData: MutableLiveData<ArrayList<Weather>>?) {
        NetworkManager.getInstance()
            ?.getKeyMethod(location, object : Callback<AccuWeatherLocationKey> {
                override fun onFailure(call: Call<AccuWeatherLocationKey>, t: Throwable) {
                    Log.e("REQUEST FAILURE", t.localizedMessage)
                }

                override fun onResponse(
                    call: Call<AccuWeatherLocationKey>,
                    response: Response<AccuWeatherLocationKey>
                ) {
                    if (!response.isSuccessful) {
                        Log.e("REQUEST FAILURE", "!response.isSuccessful")
                        return
                    }
                    var accuWeatherLocationKey = response.body()
                    var locationKey = accuWeatherLocationKey?.Key.toString()
                    if (accuWeatherLocationKey != null) {
                        getLocationWeather(accuWeatherLocationKey, locationKey, mutableLiveData)
                    }
                }
            })
    }


    fun getLocationWeather(
        accuWeatherLocationKey: AccuWeatherLocationKey, locationKey: String,
        mutableLiveData: MutableLiveData<ArrayList<Weather>>?
    ) {
        NetworkManager.getInstance()
            ?.getLocationWeather(locationKey, object : Callback<AccuWeatherLocationWeather> {
                override fun onFailure(call: Call<AccuWeatherLocationWeather>, t: Throwable) {
                    Log.e("REQUEST FAILURE", t.localizedMessage)
                }

                override fun onResponse(
                    call: Call<AccuWeatherLocationWeather>,
                    response: Response<AccuWeatherLocationWeather>
                ) {
                    println(call.request().url().toString())
                    if (!response.isSuccessful) {
                        Log.e("REQUEST FAILURE", "!response.isSuccessful")
                        return
                    }
                    var fromAW: AccuWeatherLocationWeather? = response.body()
                    var weatherList: ArrayList<Weather> = ArrayList<Weather>()
                    for (i in 0..4) {
                        var daily: DailyForecast = fromAW!!.dailyForecasts!![i]
                        var weather = Weather(
                            accuWeatherLocationKey!!.LocalizedName!!,
                            daily.Day!!.Icon.toString(),
                            daily.Night!!.Icon.toString(),
                            daily.Day!!.IconPhrase,
                            daily.Night!!.IconPhrase,
                            daily.Temperature!!.Maximum!!.Value.toString(),
                            daily.Temperature!!.Minimum!!.Value.toString(),
                            daily.Date.toString()
                        )
                        weatherList.add(weather)
                    }
                    mutableLiveData?.let {
                        it.value = weatherList
                    }
                }
            })
    }

    fun getKeyMethodByCityName(
        cityName: String,
        mutableLiveData: MutableLiveData<ArrayList<Weather>>?
    ) {

        NetworkManager.getInstance()
            ?.getKeyMethodByCityName(
                cityName,
                object : Callback<ArrayList<AccuWeatherLocationKey>> {
                    override fun onFailure(
                        call: Call<ArrayList<AccuWeatherLocationKey>>,
                        t: Throwable
                    ) {
                        Log.e("REQUEST FAILURE", t.localizedMessage)
                    }

                    override fun onResponse(
                        call: Call<ArrayList<AccuWeatherLocationKey>>,
                        response: Response<ArrayList<AccuWeatherLocationKey>>
                    ) {
                        println(call.request().url().toString())
                        if (!response.isSuccessful) {
                            Log.e("REQUEST FAILURE", "!response.isSuccessful")
                            return
                        } else {
                            var arrayList: ArrayList<AccuWeatherLocationKey>? = response.body()
                            var locationKey = arrayList!!.get(0).Key
                            if (locationKey != null) {
                                getLocationWeather(arrayList.get(0), locationKey, mutableLiveData)
                            }
                        }

                    }
                })
    }


}