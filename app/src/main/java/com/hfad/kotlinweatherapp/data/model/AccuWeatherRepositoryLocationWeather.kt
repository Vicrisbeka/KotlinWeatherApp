package com.hfad.kotlinweatherapp.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class Headline {
    @SerializedName("EffectiveDate")
    @Expose
    var EffectiveDate:Date?=null

    @SerializedName("EffectiveEpochDate")
    @Expose
    var EffectiveEpochDate:Int?=null

    @SerializedName("Severity")
    @Expose
    var Severity:Int?=null

    @SerializedName("Text")
    @Expose
    var Text:String?=null

    @SerializedName("Category")
    @Expose
    var Category:String?=null

    @SerializedName("EndDate")
    @Expose
    var EndDate:Date?=null

    @SerializedName("EndEpochDate")
    @Expose
    var EndEpochDate:Int?=null

    @SerializedName("MobileLink")
    @Expose
    var MobileLink:String?=null

    @SerializedName("Link")
    @Expose
    var Link:String?=null
}

class Minimum {
    @SerializedName("Value")
    @Expose
    var Value:Double?=null

    @SerializedName("Unit")
    @Expose
    var Unit:String?=null

    @SerializedName("UnitType")
    @Expose
    var UnitType:Int?=null
}

class Maximum {
    @SerializedName("Value")
    @Expose
    var Value:Double?=null

    @SerializedName("Unit")
    @Expose
    var Unit:String?=null

    @SerializedName("UnitType")
    @Expose
    var UnitType:Int?=null
}

class Temperature {
    @SerializedName("Minimum")
    @Expose
    var Minimum: Minimum?=null

    @SerializedName("Maximum")
    @Expose
    var Maximum: Maximum?=null
}

class Day {
    @SerializedName("Icon")
    @Expose
    var Icon:Int?=null

    @SerializedName("IconPhrase")
    @Expose
    var IconPhrase:String?=null

    @SerializedName("HasPrecipitation")
    @Expose
    var HasPrecipitation:Boolean?=null
}

class Night {
    @SerializedName("Icon")
    @Expose
    var Icon:Int?=null

    @SerializedName("IconPhrase")
    @Expose
    var IconPhrase:String?=null

    @SerializedName("HasPrecipitation")
    @Expose
    var HasPrecipitation:Boolean?=null
}

class DailyForecast {

    @SerializedName("Date")
    @Expose
    var Date:Date?=null

    @SerializedName("EpochDate")
    @Expose
    var EpochDate:Int?=null

    @SerializedName("Temperature")
    @Expose
    var Temperature: Temperature?=null

    @SerializedName("Day")
    @Expose
    var Day: Day?=null

    @SerializedName("Night")
    @Expose
    var Night: Night?=null

    @SerializedName("ID")
    @Expose
    var Sources:List<String>?=null

    @SerializedName("MobileLink")
    @Expose
    var MobileLink:String?=null

    @SerializedName("Link")
    @Expose
    var Link:String?=null
}