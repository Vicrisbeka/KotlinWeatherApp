package com.hfad.kotlinweatherapp.data.repository

import org.json.JSONArray
import org.json.JSONObject
import java.io.InputStream
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.ArrayList

class CityParser {
    var inputStream: InputStream? = null

    constructor(inputStream: InputStream) {
        this.inputStream = inputStream
    }

    fun parseCities():  ArrayList<String> {
        var mListCities = ArrayList<String>()
        var scanner = Scanner(inputStream)
        var strBuilder = StringBuilder()
        while (scanner.hasNextLine()){
            strBuilder?.append(scanner.nextLine())
        }
        inputStream?.close()

        var city = JSONArray(strBuilder.toString())
        for (i in 0..city.length()-1) {
            var cityName: JSONObject = city.getJSONObject(i)
            mListCities!!.add(cityName.getString("city"))
        }
        return mListCities
    }
}