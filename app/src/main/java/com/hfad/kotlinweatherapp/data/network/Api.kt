package com.hfad.kotlinweatherapp.data.network

import com.hfad.kotlinweatherapp.data.model.AccuWeatherLocationKey
import com.hfad.kotlinweatherapp.data.model.AccuWeatherLocationWeather
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {
    companion object {

        private const val GEO_SERCH = "locations/v1/cities/geoposition/search"
        private const val LOCATION_KEY = "forecasts/v1/daily/5day/"
        private const val CITY_SEARCH = "locations/v1/cities/search"
    }

    @GET(GEO_SERCH)
    fun getKey(
        @Query("apikey") apikey: String,
        @Query("q") location: String
    ): Call<AccuWeatherLocationKey>

    @GET("$LOCATION_KEY{locationKey}")
    fun getLocationWeather(
        @Path("locationKey") locationKey: String,
        @Query("apikey") apikey: String,
        @Query("language") language: String
    ): Call<AccuWeatherLocationWeather>

    @GET(CITY_SEARCH)
    fun getKeyByCityName(
        @Query("apikey") apikey: String,
        @Query("q") cityName: String
    ): Call<ArrayList<AccuWeatherLocationKey>>
}

