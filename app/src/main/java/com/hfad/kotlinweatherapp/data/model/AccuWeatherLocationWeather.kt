package com.hfad.kotlinweatherapp.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AccuWeatherLocationWeather {

    @SerializedName("Headline")
    @Expose
    var headline: Headline?=null

    @SerializedName("DailyForecasts")
    @Expose
    var dailyForecasts:List<DailyForecast>?=null

}