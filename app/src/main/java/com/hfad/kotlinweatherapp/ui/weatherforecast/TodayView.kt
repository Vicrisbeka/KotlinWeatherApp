package com.hfad.kotlinweatherapp.ui.weatherforecast

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.hfad.kotlinweatherapp.utils.common.Common
import com.hfad.kotlinweatherapp.data.repository.CurrentLocationListener
import com.hfad.kotlinweatherapp.data.model.Weather
import com.hfad.kotlinweatherapp.R
import com.hfad.kotlinweatherapp.ui.adapter.WeatherAdapter
import com.hfad.kotlinweatherapp.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class TodayView : AppCompatActivity() {

    private val viewModel: WeatherVM by viewModels()
    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initSearchBarAndCurrentLocation()
        configureVM()
        getDeviceLocation()
        viewModel.parseCities(resources.openRawResource(R.raw.cities_most))
    }

    private fun initSearchBarAndCurrentLocation(){
        search_btn?.setOnClickListener {
            progress_bar?.visibility = View.VISIBLE
            CurrentLocationListener.getInstance(this)?.requestingLocationUpdates = false
            viewModel.setCity(search_bar?.text.toString())
        }

        current_location_btn?.setOnClickListener {
            progress_bar?.visibility = View.VISIBLE
            CurrentLocationListener.getInstance(this)?.requestingLocationUpdates = true
        }
    }

    private fun getCurrentLocationWeather(weather: ArrayList<Weather>){
        binding?.weather = weather[0]
        var weatherAdapter = WeatherAdapter(this,0,weather)
        listview_forecast_weather?.adapter = weatherAdapter
        main_view_layout?.visibility = View.VISIBLE
        progress_bar?.visibility = View.GONE
    }

    fun getListOfCities(listOfCities: ArrayList<String>) {
        var listAdapter: ArrayAdapter<String> = ArrayAdapter(applicationContext,
            android.R.layout.simple_spinner_dropdown_item,listOfCities)
        search_bar?.setAdapter(listAdapter)
    }

    private fun configureVM() {
        val weatherObserver = Observer<ArrayList<Weather>> { weather ->
            // Update the UI
            Log.i("LiveData", "Success Call")
            getCurrentLocationWeather(weather)
        }
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        viewModel.mutableLiveData.observe(this, weatherObserver)

        val parserObserver = Observer<ArrayList<String>> {
            Log.e("Parser", "" + it)
            getListOfCities(it)
        }
        viewModel.listOfCities.observe(this, parserObserver)
    }

    private fun getDeviceLocation() {
        if (isPermissionGranted()) {
            CurrentLocationListener.getInstance(this)
                ?.observe(this, Observer<Location?> { location ->
                    if (location != null) {
                        viewModel.setLoc(location)
                    } else Toast.makeText(applicationContext, "Location is null",
                            Toast.LENGTH_SHORT).show()
                })
        } else requestPermission()
    }

    private fun isPermissionGranted(): Boolean {
        return (ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        if(!isPermissionGranted()) {
            ActivityCompat.requestPermissions(this,arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION), Common.PERMISSION_REQUEST_CODE)
        }
    }
}



