package com.hfad.kotlinweatherapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.hfad.kotlinweatherapp.data.model.Weather
import com.hfad.kotlinweatherapp.R
import com.squareup.picasso.Picasso


class WeatherAdapter(context: Context, resource: Int, objects: MutableList<Weather>) :
    ArrayAdapter<Weather>(context, resource, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var weather: Weather? = getItem(position)
        if (convertView == null) {
            var convertView = LayoutInflater.from(context).inflate(R.layout.list_view_item,parent,false)
        }
        var convertView = LayoutInflater.from(context).inflate(R.layout.list_view_item,parent,false)

        var dayImage:ImageView = convertView!!.findViewById(R.id.item_icon_day)
        var nightImage:ImageView = convertView.findViewById(R.id.item_icon_night)

        var dateView:TextView = convertView.findViewById(R.id.item_date)
        var maxTemp:TextView = convertView.findViewById(R.id.item_temperature_day)
        var minTemp:TextView = convertView.findViewById(R.id.item_temperature_night)
        var dayPhrase:TextView = convertView.findViewById(R.id.item_phrase_day)
        var nightPhrase:TextView = convertView.findViewById(R.id.item_phrase_night)

        Picasso.get().load(weather!!.iconDay).into(dayImage)
        Picasso.get().load(weather!!.iconNight).into(nightImage)

        dateView.setText(weather.dateTime)
        maxTemp.setText(weather.temperatureMax)
        minTemp.setText(weather.temperatureMin)
        dayPhrase.setText(weather.iconPhraseDay)
        nightPhrase.setText(weather.iconPhraseNight)

        return convertView
    }
}



