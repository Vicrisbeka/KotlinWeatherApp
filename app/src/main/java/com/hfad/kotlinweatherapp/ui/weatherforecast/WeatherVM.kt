package com.hfad.kotlinweatherapp.ui.weatherforecast

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hfad.kotlinweatherapp.data.repository.CityParser
import com.hfad.kotlinweatherapp.data.repository.WeatherRepository
import com.hfad.kotlinweatherapp.data.model.Weather
import kotlinx.coroutines.launch
import java.io.InputStream

class WeatherVM: ViewModel() {

    var mutableLiveData: MutableLiveData<ArrayList<Weather>> = MutableLiveData<ArrayList<Weather>>()
    var location: Location? = null
    var weatherRepository: WeatherRepository? = null
    var listOfCities: MutableLiveData<ArrayList<String>> = MutableLiveData<ArrayList<String>>()

    init {
        weatherRepository = WeatherRepository().getInstance()
    }

    fun setLoc(location: Location?) {
        if (this.location != location) {
            //TODO: use interface instead of sending link to mutableLiveData variable
            //viewModelScope.launch {
                weatherRepository?.getWeather(location, mutableLiveData)
            //}
        }
        this.location = location
    }

    fun setCity(cityName: String) {
        weatherRepository?.getWeatherByCity(cityName, mutableLiveData)
    }

    fun parseCities(inputStream: InputStream) {
        viewModelScope.launch {
            val parser = CityParser(inputStream)
            listOfCities.value = parser.parseCities()
        }
    }
}