# Weather App for juniors development program

KotlinWeatherApp is a simple weather forecast app, which uses some APIs to fetch 5 day forecast data from the AccuWeather and to fetch places and cities. The main goal of this app is to be a sample of how to build an high quality Android application that uses the Architecture components:
- LiveData
- ViewModel
- Data Binding
- Room
- Lifecycle-Aware
- DataStore
- Paging
- Work Manager
- Saving UI States
- Kotlin Coroutines
